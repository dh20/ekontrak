 
<style type="text/css" media="screen">
 .dataTables_filter{
           float: right
    }     
    table.dataTable{
      text-align: center; 
    }
    table.dataTable th:nth-child(5) {
      width:90px;
      text-align: center; 
      
    }
    
   
   
 </style>

<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <!-- <h3>Users <small>Some examples to get you started</small></h3> -->

              </div>
              </div>


                <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-lg-12">
                <div class="x_panel">
                  <div class="x_title">
                  	
                    <h2>Data Vendor<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                    	<button type="button" onClick="location.href='<?= base_url('/datamaster/viewTambahDataVendor');?>';" class="btn btn-primary">Tambah Data</button><!-- 
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li> -->
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  
          <div class="x_content">
          <div class="row">
          <div class="col-sm-12">
          <div class="card-box table-responsive">	

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			
			<div class="x_content">
				<p class="text-muted font-13 m-b-30">
					</code>
				</p>
				<!-- <table id="datatable-buttons" class="table table-striped table-bordered"> -->
					<table id="datatable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>No</th>
							<th id="vendor">Kode Vendor</th>
							<th >Vendor</th>
							<th id="keterangan">Keterangan</th>
							<th>Aksi </th>
							
						</tr>
					</thead>


					<tbody>
						
						<?php
							$i=1;
							foreach ($data as $row) {
						?>
						<tr>
							<td><?= $i ?></td>
							<td><?= $row->kodeVendor ?></td>
							<td><?= $row->namaVendor ?></td>
							<td><?= $row->keterangan ?></td>
							<td>
							<a href="viewEditDataVendor/<?= $row->kodeVendor?>" class="edit" title="" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil-square-o  fa-lg"></i></a>  &nbsp; <a href="deleteDataVendor/<?= $row->kodeVendor?>" onclick="return confirm('Are you sure you want to delete this item?');" class="delete" title="" data-toggle="tooltip" data-original-title="Delete"><i class="glyphicon glyphicon-trash"></i></a>
							</td>
						</tr>
						<?php $i++;} ?>
						
					</tbody>
				</table>
			</div>
		</div>
	</div>

	


			</div>
		</div>
	</div>
</div>