 
<style type="text/css" media="screen">
 	.dataTables_filter{
           float: right
    }     
    table.dataTable{
      text-align: center; 
    }
    table.dataTable th:nth-child(10) {
      width:90px;
      text-align: center; 
      
    }
    table.dataTable td:nth-child(9) {
      text-align: justify !important; 
      
    }
   
   
 </style>

<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <!-- <h3>Users <small>Some examples to get you started</small></h3> -->

              </div>
              </div>


                <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-lg-12">
                <div class="x_panel">
                  <div class="x_title">
                  	
                    <h2>Amendemen Kontrak<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                    	<button type="button" onClick="location.href='<?= base_url('datamaster/viewTambahDataAmendKontrak');?>';" class="btn btn-primary">Tambah Data</button><!-- 
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li> -->
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  
          <div class="x_content">
          <div class="row">
          <div class="col-sm-12">
          <div class="card-box table-responsive">


				 <table id="datatable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th id="no">No</th>
							<th>Kode Vendor</th>
							<th >Nomor Kontrak Induk</th>
							<th>Nomor Amend</th>
							<th>Tanggal Amend</th>
							<!--<th id="amend">Amend</th>-->
							<th id="tanggalSelesai">Tanggal Mulai</th>
							<th id="tanggalSelesai">Tanggal Selesai</th>
							<th id="tanggalSelesai">Doc</th>
							<th>Resume</th>
							<!-- <th id="status">Status</td> -->
							<th id="aksi"  width="3%"> Aksi</th>
							
						</tr>
					</thead>


					<tbody>
						
						<?php
							$i=1;
							foreach ($data as $row) {
						?>
						<tr>
							<td><?= $i ?></td>
							<td><?= $row->kodeVendor ?></td>
							<td><?= $row->nomorKontrak ?></td>
							<td><?= $row->nomorAmend ?></td>
							<td><?= $row->tanggalAmend ?></td>
						    <!--<th style="text-align:center;"><?= $row->amendemen; ?></th>-->
							<td><?php echo $row->tanggalMulai; ?></td>
							<td><?= $row->tanggalSelesai ?></td>
							<td style="text-align:center;">
								 <?php echo '<a href="" data-toggle="modal" data-target="#myModal" 
					             onclick="showGame(\''. $row->lampiranAmend.'\')" >' ?>
					              <i class="fa fa-file fa-lg" aria-hidden="true" ></i></a>
<!-- 
								<a href="<?= $row->lampiranAmend ?>" target="_blank"><i class="fa fa-file fa-lg" aria-hidden="true"></i></a> -->

							</td>
							<td><?= $row->ketTambahan ?></td>
							<!-- <td align="center"> -->
							  <!--  <?php if($_SESSION['level']=="officer"){  
							     
							         if($row->validasi==1){
							             ?>
							             <a href="validasiAmendKontrak/<?= $row->idAmendKontrak?>/1" onclick="return confirm('Apakah anda ingin menvalidasi kontrak ini?');" class="btn btn-info" title="" data-toggle="tooltip" >Valid</i></a>
							             <a href="validasiAmendKontrak/<?= $row->idAmendKontrak?>/0" onclick="return confirm('Apakah anda ingin mengembalikan kontrak ini?');" class="btn btn-warning" title="" data-toggle="tooltip" >Tidak Valid</i></a>
							             
							             <?php
							         }else{
							            
							             ?>
							            <?php
							            if($row->validasi==3){
							                 
							                echo "<img src='https://www.flaticon.com/svg/static/icons/svg/845/845646.svg' width='20px' height='20px' >";
							            }else if($row->validasi==2)
    							            {
    							             echo "<img src='https://www.flaticon.com/svg/static/icons/svg/889/889843.svg' width='20px' height='20px' >";    
    							            }else if($row->validasi==0){
							                echo "Dikembalikan";    
							            }
							                
							            }
							    }
							    
							        if($_SESSION['level']=="kadis"){  
							         if($row->validasi==2){
							             ?>
							             <a href="validasiAmendKontrak/<?= $row->idAmendKontrak?>/1" onclick="return confirm('Apakah anda ingin menvalidasi kontrak ini?');" class="btn btn-info" title="" data-toggle="tooltip"  >Valid</i></a>
                                         <a href="validasiAmendKontrak/<?= $row->idAmendKontrak?>/0" onclick="return confirm('Apakah anda ingin mengembalikan kontrak ini?');" class="btn btn-warning" title="" data-toggle="tooltip" >Tidak Valid</i></a>							             
							             <?php
							         }else{
							             ?>
							 
							         <?php
							            if($row->validasi==3){
							               
							                echo "<img src='https://www.flaticon.com/svg/static/icons/svg/845/845646.svg' width='20px' height='20px' >";
							            }else if($row->validasi==0){
							                echo "Dikembalikan";    
							         		   } 
							      		   }
							  			  }
							    
						            
						            if($_SESSION['level']=="vendor"){ 
							        
							            
							            if($row->validasi==3){
							                echo " <img src='https://www.flaticon.com/svg/static/icons/svg/845/845646.svg' width='20px' height='20px' >";
							            }else{
							                echo "proses validasi";
							            }; 
							        
							    }	    
							   ?> 
					        	 -->
							 
							<!-- </td> -->
							<td>
							<a  href="viewEditDataAmendKontrak/<?= str_replace("/",".",$row->idAmendKontrak);?>"  class="edit" title="" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil-square-o  fa-lg" aria-hidden="true"></i></a>  &nbsp; <a href="deleteDataAmendKontrak/<?= $row->idAmendKontrak?>" onclick="return confirm('Are you sure you want to delete this item?');" class="delete" title="" data-toggle="tooltip" data-original-title="Delete"><i class="glyphicon glyphicon-trash"></i></a>
							</td>

						</tr>
						<?php $i++;} ?>
					</tbody>
				</table>
				<div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                       <h5 class="modal-title" id="exampleModalLabel">Lampiran</h5>
                          <!-- <h5 class="modal-title" id="exampleModalLabel">Amend Kontrak</h5> -->
                          <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button> -->
                 
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <!-- <h4 class="modal-title">Modal Header</h4> -->
                        </div>
                        <div class="modal-body">

                            <embed id="pdfRead" src="https://i.stack.imgur.com/ATB3o.gif" frameborder="0" width="100%" height="500px" allow="autoplay">
                      
                            <!-- <iframe id="pdfRead" src="https://drive.google.com/file/d/1Ni4n7YM4n7RF35peKhXpzxS8pThW1YPK/preview" width="100%" height="500px" allow="autoplay"></iframe> -->

                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
			</div>
		</div>
	</div>

	
<script type="text/javascript">
  

  function showGame(val){
          console.log(val);
          // document.getElementById("pdfRead").src = val;
          // val="";

          // var source=whichgame.getAttribute(val);
          var game=document.getElementById("pdfRead");
          game.src = val;
          var clone=game.cloneNode(true);
          clone.setAttribute('src',val);
          game.parentNode.replaceChild(clone,game)
        }


</script>
<script src="https://github.com/pipwerks/PDFObject/blob/master/pdfobject.min.js"></script>

 