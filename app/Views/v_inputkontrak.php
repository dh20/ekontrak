  <style>
  
  </style>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
        <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <!-- <div class="box-header with-border"> -->
              <!-- <h3 class="box-title">Quick Example</h3> -->
            <!-- </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            
        <br>
        <form action="https://e-kontrak.patas2018.com/DataMaster/tambahDataVendor" method="post" id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left">
        
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kode Vendor <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="kodeVendor">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Vendor <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="last-name" required="required" class="form-control col-md-7 col-xs-12" name="namaVendor">
            </div>

          </div>
          <div class="form-group">
            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Keterangan</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="keterangan"> </textarea>
            </div>
          </div>
          
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              
              
              <button onclick="window.location.href='https://e-kontrak.patas2018.com/DataMaster/viewDataVendor'" class="btn btn-primary" type="button">Cancel</button>
              <button class="btn btn-primary" type="reset">Reset</button>

              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </div>
        </form>
      </div>
          </div>
          <!-- /.box -->

            </div>
            <!-- /.box-body -->
          
          <!-- /.box -->
        
        <!--/.col (right) -->
       
          <!-- /.box -->

        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->


  