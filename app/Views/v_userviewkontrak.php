<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo base_url('asset/build/images/favicon.png')?>" type="image/x-icon">


    <title>E-Kontrak Userview </title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url('asset/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url('asset/vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url('asset/vendors/nprogress/nprogress.css'); ?>" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url('asset/vendors/iCheck/skins/flat/green.css'); ?>" rel="stylesheet">
  
    <!-- bootstrap-progressbar -->
    <link href="<?php echo base_url('asset/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css'); ?>" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo base_url('asset/vendors/jqvmap/dist/jqvmap.min.css'); ?>" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url('asset/vendors/bootstrap-daterangepicker/daterangepicker.css'); ?>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url('asset/build/css/custom.min.css'); ?>" rel="stylesheet">
  <style type="text/css" media="screen">
    body{
      background-image: linear-gradient(to right bottom, #478fd1, #598fd9, #6d8ee0, #818de4, #978ae7, #8c96f1, #81a1f9, #77acff, #46c3ff, #00d8ff, #21ebff, #5ffbf1);
    }  
    .bg-tabel {

      margin: 2%;
    }
    .dataTables_filter{
           float: right
    }
    h5{
      color: #7ba8fd;
    }
         
    .navbar-light .navbar-text a {
       color: #7ba8fd;
    }    
         
  
  </style>
  </head>

  <body >
    <nav class="navbar navbar-expand-lg navbar-light bg-white">
  <a class="navbar-brand" href="#"><h5>
    <img src="<?php echo base_url('asset/build/images/Logo.png')?>" width="auto" height="30"/> 

  </h5></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <!-- <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Features</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Pricing</a>
      </li> -->
    </ul>
    <span class="navbar-text">
      <h6><a href="<?= base_url('Login/logout');?>">Logout</a></h6>
    </span>
  </div>
</nav>


    <div class="bg-tabel">
    <div class="container body">
      <div class="main_container">
        
        <!-- /top navigation -->
        

             <!--  <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                  </div>
                </div> -->
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-lg-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><small> Dinas Tax-Verification </small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <!-- <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a> -->
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  
          <div class="x_content">
          <div class="row">
          <div class="col-sm-12">
          <div class="card-box table-responsive">
            
            <table id="datatable" class="table table-striped table-bordered">
              <thead>
              <tr>
              <th >No</th>
              <th >Kode Vendor</th>
              <th >Nama Vendor</th>
              <th>Nomor Kontrak</th>
              <th >Tanggal Kontrak</th>
              <th >Tanggal Mulai</th>
              <th >Tanggal Selesai</th>
              <th>Waktu</th>
              <th>Kategori</th>
              <th>Judul</th>
              <th>Resume</th>
              <th >Kontrak Induk</th>
              <th>Amendkontrak</th>
              
              
              
            </tr>
          </thead>


          <tbody>
            
            <?php
              foreach ($data as $row) {
            ?>
            <tr>
              
              <td><?= $row['index']+1; ?></td>
              <td><?= $row['kodeVendor'] ?></td>
              <td><?= $row['namaVendor'] ?></td>
              <td><?= $row['nomorKontrak'] ?></td>
              <td><?= $row['tanggalKontrak'] ?></td>
              <td><?= $row['tanggalMulai'] ?></td>
              <td><?= $row['tanggalSelesai'] ?></td>
              <td><?= $row['jangkaWaktuPembayaran'] ?></td>
              <td><?= $row['kategori'] ?></td>
              <td><?= $row['judul'] ?></td>
              <td>
              <?php 
                
              ?>
              <!-- <a href=""  data-toggle="modal" data-target="#resumeModal" > -->

                <a href="" data-toggle="modal" data-target="#resumeModal" 
                onclick='showResume(<?php echo JSON_ENCODE(array($row['ketTambahan'],$row['resume'],$row['amendemen'])); ?> )' >
                <i class="fa fa-search fa-lg" aria-hidden="true"></i></a>

             

              </td>
              <!-- Modal -->
              

              <td style="text-align:center;">
                <?php echo '<a href="" data-toggle="modal" data-target="#myModal" 
             onclick="showGame(\''. $row['lampiranKontrak'].'\')" >' ?>
              <i class="fa fa-file fa-lg" aria-hidden="true" ></i></a>
                
              </td>

              <td><?php 

              foreach ($row['lampiranAmendKontrak']  as $key ) {
                ?>
                  
             <?php echo '<a href="" data-toggle="modal" data-target="#myModal" 
             onclick="showGame(\''.$key.'\')" />' ?>

            <i class="fa fa-file fa-lg" aria-hidden="true" ></i></a>

                  <!-- <embed src="sample.pdf" frameborder="0" width="100%" height="400px"> -->



              <?php }

              ?>          

              

            </td>


                    
                            

            </tr>
            <?php } ?>
            
          </tbody>
                 <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                          <div class="modal-header">
                          <!-- <h5 class="modal-title" id="exampleModalLabel">Amend Kontrak</h5> -->
                          <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button> -->
                        </div>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <!-- <h4 class="modal-title">Modal Header</h4> -->
                        </div>
                        <div class="modal-body">

                            <embed id="pdfRead" src="https://i.stack.imgur.com/ATB3o.gif" frameborder="0" width="100%" height="500px">

                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

             <div class="modal fade" id="resumeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Resume</h5>
                      <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button> -->
                    </div>
                    <div class="modal-body">
                      
                        <textarea style="width:100%; height:300px;" id="idResume" readonly="true">
                          
                        </textarea>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                    </div>
                  </div>
                </div>
              </div>


           
          </table>
                  </div>
                  </div>
              </div>
            </div>
                </div>
              </div>

                
          
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
         <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.2);color: #FFFF;">
    © 2021 Copyright:
    <a class="text-reset fw-bold" href="https://mdbootstrap.com/">Div. Tax and Verification</a>
  </div>
        <!-- /footer content -->
      </div>
    </div>

    <script type="text/javascript">
        function showResume(val){
          var txt = document.getElementById("idResume");
          txt.style.fontWeight = 'bold';

           var text="";
          if(val[1].length==0){
            txt.innerHTML = "Kontrak Induk"+"\n"; 
            txt.innerHTML += val[0];
          }else{
            txt.innerHTML = "Kontrak Induk"+"\n"; 
            txt.innerHTML += val[0];
            txt.innerHTML += "\n\n";
            txt.innerHTML += "Amend Kontrak"+"\n"; 
            // console.log(val[1].length);
            for (var i = 0; i < val[1].length ; i++) {
              txt.innerHTML += "Ke-"+val[2][i]+" - "+val[1][i];
              txt.innerHTML += "\n";
            }
            

            // text=val[0]+"\n"+val[1]

          }

         
          // document.getElementById("resultlist").innerHTML = val[1];
          
          // listElement = document.createElement("li"),
          // container = document.querySelector("#resultlist"); 
          // listElement.innerHTML = val[1];
          // container.appendChild(listElement);
           
        }        
        function showGame(val){
          console.log(val);
          // document.getElementById("pdfRead").src = val;
          // val="";

          // var source=whichgame.getAttribute(val);
          var game=document.getElementById("pdfRead");
          game.src = val;
          var clone=game.cloneNode(true);
          clone.setAttribute('src',val);
          game.parentNode.replaceChild(clone,game)
        }

    </script>
    <script src="https://github.com/pipwerks/PDFObject/blob/master/pdfobject.min.js"></script>

     <!-- jQuery -->
    <script src="<?php echo base_url('asset/vendors/jquery/dist/jquery.min.js'); ?>"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url('asset/vendors/bootstrap/dist/js/bootstrap.bundle.min.js'); ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url('asset/vendors/fastclick/lib/fastclick.js'); ?>"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url('asset/vendors/nprogress/nprogress.js'); ?>"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url('asset/vendors/Chart.js/dist/Chart.min.js'); ?>"></script>
    <!-- gauge.js -->
    <script src="<?php echo base_url('asset/vendors/gauge.js/dist/gauge.min.js'); ?>"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url('asset/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js'); ?>"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url('asset/vendors/iCheck/icheck.min.js'); ?>"></script>
    <!-- Skycons -->
    <script src="<?php echo base_url('asset/vendors/skycons/skycons.js'); ?>"></script>
    <!-- Flot -->
    <script src="<?php echo base_url('asset/vendors/Flot/jquery.flot.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/Flot/jquery.flot.pie.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/Flot/jquery.flot.time.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/Flot/jquery.flot.stack.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/Flot/jquery.flot.resize.js'); ?>"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url('asset/vendors/flot.orderbars/js/jquery.flot.orderBars.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/flot-spline/js/jquery.flot.spline.min.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/flot.curvedlines/curvedLines.js'); ?>"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url('asset/vendors/DateJS/build/date.js'); ?>"></script>
    <!-- JQVMap -->
    <script src="<?php echo base_url('asset/vendors/jqvmap/dist/jquery.vmap.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/jqvmap/dist/maps/jquery.vmap.world.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js'); ?>"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url('asset/vendors/moment/min/moment.min.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url('asset/build/js/custom.min.js'); ?>"></script>

    <!-- Datatables -->
    <script src="<?php echo base_url('asset/vendors/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/datatables.net-buttons/js/dataTables.buttons.min.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/datatables.net-buttons/js/buttons.flash.min.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/datatables.net-buttons/js/buttons.html5.min.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/datatables.net-buttons/js/buttons.print.min.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/datatables.net-responsive/js/dataTables.responsive.min.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/datatables.net-scroller/js/dataTables.scroller.min.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/jszip/dist/jszip.min.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/pdfmake/build/pdfmake.min.js'); ?>"></script>
    <script src="<?php echo base_url('asset/vendors/pdfmake/build/vfs_fonts.js'); ?>"></script>

   
  </body>
</html>