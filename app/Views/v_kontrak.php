 
<style type="text/css" media="screen">
 .dataTables_filter{
           float: right
    }     
    table.dataTable{
      text-align: center; 
    }
    table.dataTable td:nth-child(5) {
      width: 10px;
      text-align: justify; 
      
    }
     
    
 
   
 </style>


  <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <!-- <h3>Users <small>Some examples to get you started</small></h3> -->
              </div>

              <!-- <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div> -->
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-lg-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Kontrak Induk<small></small></h2>

                    <ul class="nav navbar-right panel_toolbox">
                      <button type="button" onClick="location.href='<?= base_url('datamaster/viewTambahDataKontrak');?>';" class="btn btn-primary">Tambah Data</button>

                    <!--   <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li> -->
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  
          <div class="x_content">
          <div class="row">
          <div class="col-sm-12">
          <div class="card-box table-responsive">
            
            <table id="datatable" class="table table-striped table-bordered">
              <thead>
              <tr>
              <th id="no">No</th>
              <th id="vendor">Kode Vendor</th>
              <th id="vendor">Nama Vendor</th>
              <th>Nomor Kontrak</th>
              <th>Judul</th>
              <th id="tanggalSelesai">Tanggal Kontrak</th>
              <th id="tanggalSelesai">Tanggal Mulai</th>
              <th id="tanggalSelesai">Tanggal Selesai</th>
              <th id="tanggalSelesai">Doc</th>
              <th>Waktu</th>
              <th>Kategori</th>
              <th>Resume</th>
              <!-- <th id="status">Status</td> -->
              <th id="aksi"  width="3%"> Aksi</th>
              
            </tr>
          </thead>


          <tbody>
            
            <?php
              $i=1;
              foreach ($data as $row) {
            ?>
            <tr>
              <td><?= $i ?></td>
              <td><?= $row->kodeVendor ?></td>
                <td><?=$row->namaVendor ?></td>
              <td><?= $row->nomorKontrak ?></td>
              <td><?= $row->judul ?></td>
              <td><?= $row->tanggalKontrak ?></td>
              <td><?= $row->tanggalMulai ?></td>
              <td><?= $row->tanggalSelesai ?></td>
              <!-- <td style="text-align:center;">

                <a href="<?= $row->lampiranKontrak ?>" target="_blank"> -->
              <td style="text-align:center;">
                <?php echo '<a href="" data-toggle="modal" data-target="#myModal" 
             onclick="showGame(\''. $row->lampiranKontrak.'\')" >' ?>
              <i class="fa fa-file fa-lg" aria-hidden="true" ></i></a>
                
              </td>


              <!-- <i class="fa fa-file" aria-hidden="true"></i></a></td> -->
              <td><?= $row->jangkaWaktuPembayaran ?></td>
              <td><?= $row->kategori ?></td>
              <td>
                
                 <a href="" data-toggle="modal" data-target="#resumeModal" 
                onclick='showResume(<?php echo JSON_ENCODE(array($row->ketTambahan)); ?> )' >
                <i class="fa fa-search fa-lg" aria-hidden="true"></i></a>
                


              </td>
              <!-- <td>-</td> -->
              <!-- <td align="center">
                 <?php if($_SESSION['level']=="officer"){  
                       if($row->validasi<2 && $row->validasi>0){
                           ?>
                           
                           <a href="validasiKontrak/<?= $row->idKontrak?>/1" onclick="return confirm('Apakah anda ingin menvalidasi kontrak ini?');" class="btn btn-info" title="" data-toggle="tooltip" >Valid</i></a>
                           <a href="validasiKontrak/<?= $row->idKontrak?>/0" onclick="return confirm('Apakah anda ingin mengembalikan kontrak ini?');" class="btn btn-warning" title="" data-toggle="tooltip" >Tidak Valid</i></a>
                           <?php
                          
                           
                       }else{
                           ?>
               
                       <?php
                          if($row->validasi>2){
                              echo " <img src='https://www.flaticon.com/svg/static/icons/svg/845/845646.svg' width='20px' height='20px' >";
                          }else if($row->validasi==2)
                              {
                               echo " <img src='https://www.flaticon.com/svg/static/icons/svg/889/889843.svg' width='20px' height='20px' >";    
                              }else if($row->validasi==0){
                              echo "Dikembalikan";    
                          }
                              
                          }
                      }
                  
                      if($_SESSION['level']=="kadis"){  
                       if($row->validasi==2){
                           ?>
                           <a href="validasiKontrak/<?= $row->idKontrak?>/1" onclick="return confirm('Apakah anda ingin menvalidasi kontrak ini?');" class="btn btn-info" title="" data-toggle="tooltip"  >Validasi</i></a>
                           <a href="validasiKontrak/<?= $row->idKontrak?>/0" onclick="return confirm('Apakah anda ingin mengembalikan kontrak ini?');" class="btn btn-warning" title="" data-toggle="tooltip" >Tidak Valid</i></a>
                           <?php
                       }else{
                           ?>
               
                       <?php
                          if($row->validasi==3){
                              echo " <img src='https://www.flaticon.com/svg/static/icons/svg/845/845646.svg' width='20px' height='20px' >";
                          }else if($row->validasi==0){
                              echo "Dikembalikan";    
                          }
                       }
                  }
                  if($_SESSION['level']=="vendor"){ 
                      
                      
                          if($row->validasi==3){
                              echo " <img src='https://www.flaticon.com/svg/static/icons/svg/845/845646.svg' width='20px' height='20px' >";
                          }else if($row->validasi==0){
                              echo "Dikembalikan";    
                          }else{
                              echo "proses validasi";
                          }; 
                          
                          
                      
                  }
                  
                 ?> 
                    
               
              </td>    -->
              <td>
              <a  href="viewEditDataKontrak/<?= str_replace("/",".",$row->idKontrak);?>"  class="edit" title="" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil-square-o  fa-lg" aria-hidden="true"></i></a>  &nbsp;  <a href="deleteDataKontrak/<?= $row->idKontrak?>" onclick="return confirm('Are you sure you want to delete this item?');" class="delete" title="" data-toggle="tooltip" data-original-title="Delete"><i class="glyphicon glyphicon-trash"></i></a>
              </td>

            </tr>
            <?php $i++;} ?>
            
          </tbody>
                



           
          </table>
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Lampiran</h5>
                          
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <!-- <h4 class="modal-title">Modal Header</h4> -->
                        </div>
                        <div class="modal-body">

                            <embed id="pdfRead" src="https://i.stack.imgur.com/ATB3o.gif" frameborder="0" width="100%" height="500px" allow="autoplay">
                      
                            <!-- <iframe id="pdfRead" src="https://drive.google.com/file/d/1Ni4n7YM4n7RF35peKhXpzxS8pThW1YPK/preview" width="100%" height="500px" allow="autoplay"></iframe> -->

                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="modal fade" id="resumeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Resume</h5>
                      <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button> -->
                    </div>
                    <div class="modal-body">
                      
                        <textarea style="width:100%; height:300px;" id="idResume" readonly="true">
                          
                        </textarea>

                    </div>
                 <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      
                    </div>
                  </div>
                </div>
              </div>

                  </div>
                  </div>
              </div>
            </div>
                </div>
              </div>

                
          
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<script type="text/javascript">
  function showGame(val){
          console.log(val);
          // document.getElementById("pdfRead").src = val;
          // val="";

          // var source=whichgame.getAttribute(val);
          var game=document.getElementById("pdfRead");
          game.src = val;
          var clone=game.cloneNode(true);
          clone.setAttribute('src',val);
          game.parentNode.replaceChild(clone,game)
        }
        
  function showResume(val){
        console.log(val);
        document.getElementById("idResume").innerHTML=val;
       
          
  }
 


</script>
<script src="https://github.com/pipwerks/PDFObject/blob/master/pdfobject.min.js"></script>