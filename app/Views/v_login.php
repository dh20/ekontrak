<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css" sizes="10x10">

    <link rel="stylesheet" href="css/style.css">

    <link rel="icon" href="Favicon.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    
    <link rel="shortcut icon" href="<?php echo base_url('asset/build/images/favicon.png')?>" type="image/x-icon">
 
    

    <title> E-Kontrak : Login </title>  

<style type="text/css" media="screen">

img {
    margin-left:7%;
    vertical-align: center;
    border-style: none;
    margin-bottom: 25px;
}

@import url(https://fonts.googleapis.com/css?family=Raleway:300,400,600);


.background {
  
}

.layer {
    background-color: rgba(44, 62, 80,0.0);
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}


body{
    background-image: url(<?php echo base_url("asset/build/images/lg.jpg") ?>);
    height: 100%;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;

    /*margin: 0;
    font-size: .9rem;
    font-weight: 400;
    line-height: 1.6;
    color: #212529;
    text-align: left;*/
    /*background-color: #16a085;*/
/*    background-image: url("https://www.vtexperts.com/wp-content/uploads/2016/07/google-map-background-1900x1170.png");
     background-color: red;*/
}
.login-form{
    margin-top:6% !important;

}

  
    .is{
	    opacity: 1;
	    animation-name: fadeInOpacity;
    	animation-iteration-count: 1;
    	animation-timing-function: ease-in;
    	animation-duration: 0.5s;
	}
	
	@keyframes fadeInOpacity {
	0% {
	    margin-top:-60px;
		opacity: 0;
	}
	100% {
		opacity: 1;
		margin-top: 0;
	}
    


  
</style>
</head>
<body>


<div class="background">
    <div class="layer">
<div class="container">
<div class="is">
<main class="login-form">
    <div class="cotainer">
        <div  class="row justify-content-end">
           
            <div class="col-md-5 ">
                
            <br>
                <div class="card">
                    
                    <div class="card-header text-center text-white">
                    
                    <img  src="<?php echo base_url('/asset/build/images/Logo.png')?>" width="50%" />

                    </div>

                    <div class="card-body">
                        <form action="<?php echo base_url('Login'); ?>" method="POST">
                            <div class="form-group row">
                                
                                <div class="col-md-12">
                                    <input type="text" id="email_address" class="form-control" name="user" required placeholder="username" autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                
                                <div class="col-md-12">
                                    <input type="password" id="password" class="form-control" name="pass" required placeholder="password" >
                                </div>
                            </div>
                            <div class="form-group row">

                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-block">
                                    Login
                                </button>
                                <a href="#" class="btn btn-link">
                                    Lupa Password ? Hubungi Admin
                                </a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
     </div>
</div>
</div>
</main>







</body>
</html>




