
<style type="text/css" media="screen">
	#datepicker{
		 width: 15.1em; padding: .2em .2em;
		 height: 2.4em;
		 font-size: 14px;
	
	}
	
	
</style>
<?php if($_SESSION['status']==100){
?>


<div id="alrt" class="alert alert-success" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success!</strong> Data Kontrak Berhasil diupdate!
</div>

	<?php
	};?>



  <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <!-- <h3>Users <small>Some examples to get you started</small></h3> -->
              </div>

              <!-- <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div> -->
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-lg-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Kontrak Induk<small></small></h2>

                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>



				<!-- First Section one Column -->
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
									
							<div class="x_content">
								<br />
								

								<form  action="<?= base_url()?>/datamaster/simpanDataKontrak" method="post" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" novalidate>
								

				                <input type="hidden" name="idKontrak" value="<?= $data[0]->idKontrak; ?>"/>
								<div class="item form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12">Vendor</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<select class="form-control" name="kodeVendor">
											<?php foreach ($vendor as $val): ?>
												
												<option <?php if( $val->kodeVendor==$data[0]->kodeVendor){
												 echo "selected";} ?> > <?= $val->namaVendor; ?> </option>
											<?php endforeach ?>
												
											</select>
										</div>
									</div>
									<div class="item form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nomor Kontrak <span class="required">*</span>
										</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											
										   <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" value="<?= $data[0]->nomorKontrak; ?>" name="nomorKontrak" />
										
										</div>
									</div>
									<div class="item form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal Kontrak <span class="required">*</span>
										</label>
										<div class="col-md-3 col-sm-3 col-xs-12">
										<input type="date" name="tanggalKontrak" id="datepicker" value="<?= $data[0]->tanggalKontrak?>"/> 
										</div>


									</div>

									<div class="item form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal Mulai / Tanggal Akhir <span class="required">*</span>
										</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
										<!-- 	<input type="text" id="last-name"  required="required" class="form-control col-md-7 col-xs-12" value="<?= $data[0]->tanggalMulai; ?>" name="namaVendor"> -->
										<input type="date" name="tanggalMulai" id="datepicker" value="<?= $data[0]->tanggalMulai?>"/> /
										<input type="date" name="tanggalSelesai" id="datepicker" value="<?= $data[0]->tanggalSelesai?>"/>
										</div>


									</div>
										<div class="item form-group">
                    						<label  for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Judul Kontrak <span class="required"></span> </label>
                    						<div class="col-md-6 col-sm-6 col-xs-12">
                    							<input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" value="<?= $data[0]->judul;  ?>"  name="judul"/>
                     
                    						</div>
                    					</div>
                    					
									<div class="item form-group">
										
										<label  for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Lampiran <span class="required">* Link </span> </label>


										<div class="col-md-6 col-sm-6 col-xs-12">
											 										 
											
											 <a  data-toggle="tooltip" data-original-title="click to view"   target="_blank" href="<?=$data[0]->lampiranKontrak; ?>">
											 <span  class="badge badge-success"><?php echo basename($data[0]->lampiranKontrak); ?> </span></a>
						
												<input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12"  name="lampiran" value="<?php echo $data[0]->lampiranKontrak ?>"/>
														 



											<!-- input kontrak by upload -->
											<!-- <input id="input-b1" name="userfile" type="file" class="file" data-browse-on-zone-click="true" accept="application/pdf" > -->
											<!-- end -->



											<!-- <div class="input-group">
												<a  target="_blank" href="<?= $data[0]->lampiranKontrak; ?>">
												<input 
												id="lampr" data-toggle="tooltip" data-original-title="click to view"
												type="text" readonly class="form-control" value="<?= $data[0]->lampiranKontrak; ?>"></a>
												<span class="input-group-btn">

												<button type="button" class="btn btn-primary">Ganti</button>
												</span>
											</div> -->

										</div>
									</div>

									<div class="item form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jangka Waktu Pembayaran <span class="required">*</span>
										</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" value="<?= $data[0]->jangkaWaktuPembayaran;  ?>" name="jangkaWaktuPembayaran" />
										</div>
									</div>


									<div class="item form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Keterangan Tambahan 
										</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<textarea class="form-control" name="ketTambahan"  rows="3"  id="comment"><?= $data[0]->ketTambahan;  ?></textarea>
										</div>
									</div>

									<div class="item form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<select class="form-control" name="kategori">
												<option value="Jasa" <?php if($data[0]->kategori == "Jasa"){ echo "selected";} ?> > Jasa </option>
												<option value="Barang" <?php if($data[0]->kategori == "Barang"){ echo "selected";} ?>> Barang </option>
											</select>
										</div>
									</div>


									
									<div class="ln_solid"></div>
									<div class="item form-group">
										<div class="col-md-6 col-sm-6 offset-md-3">
											
											
											<button onclick="window.location.href='<?= base_url('datamaster/view_kontrak')?>'" class="btn btn-primary" type="button">Back</button>
											<button class="btn btn-primary" type="reset">Reset</button>

											<button type="submit" class="btn btn-success">Submit</button>
										</div>
									</div>
								</form>
							</div>
						</div>
	</div>
</div>

<script type="text/javascript">


	window.setTimeout(function() {
    $(".alert").fadeTo(200, 0).slideUp(200, function(){
        $(this).remove(); 
    });
}, 1500);


</script>
