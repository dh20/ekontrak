<?php
namespace App\Controllers;
use App\Models\M_userview;
use App\Models\Kontrak;



/**
* This is Example Controller
*/
class Userview extends BaseController
{
	
	function __construct()
	{
		 
		
		
	}
	
	function index() {
		 
// 		$this->load->view('v_header');	
		echo view('v_userView');
// 		$this->load->view('v_footer');	
	}

	function userviewkontrak(){
		$session = session();
		if($session->status==1){
	
        $session->set(array('level'=>'officer'));
        $kontrak = new Kontrak();
        $data= [
        	'data'=>$kontrak->getDataKontrakAmend()
        ]; 
		

        // $kontrak->getData());
	  	echo view('v_userviewkontrak',$data);
    	}else{
				return redirect()->to(site_url("Login"));
    	}
		 
	}

	
	public function apidata(){

		$data= new M_userview();
		

		
	    $i=0;
	    $i2=0;
		$dt=array();
	
		foreach($data->getData() as $val){
		   	$dt2=array();
		   
		    $dataAmend=$data->getDataById($val->idKontrak);
		    $idKontrak="";
		    foreach($dataAmend as $val2){
		      $dt2[$i2]=array(
		           'idAmendKontrak'=>$val2->idAmendKontrak,
		           'idKontrak'=>$val2->idKontrak,
		           'keterangan'=>$val2->ketTambahan,
		           'nomorAmend'=>$val2->nomorAmend,
		           'lampiranAmend'=>$val2->lampiranAmend,
		           'amendemen'=>$val2->amendemen
		             );
		             $idKontrak=$val2->idKontrak;
		      $i2++;
		      
		    }
		   
		    
		    if($val->idKontrak==$idKontrak){
		         $dt2=$dt2;
		     
		    }else
		    {
		        $dt2=0;
		    }
		    
		     $dt[$i]=array(
		         'idKontrak'=>$val->idKontrak,
		         'lampiranKontrak'=>$val->lampiranKontrak,
		          'namaVendor'=>$val->namaVendor,
		         'kodeVendor'=>$val->kodeVendor,
		         'nomorKontrak'=>$val->nomorKontrak,
		         'tanggalMulai'=>$val->tanggalMulai,
		         'tanggalSelesai'=>$val->tanggalSelesai,
		         'kategori'=> $val->kategori,
		         'ketTambahan'=>$val->ketTambahan,
		          'resume'=> $dt2
		         );
		 
	    
		  $i2=0; 
		  $i++;
           
		}
		echo json_encode(array('data'=>$dt));
// 		echo json_encode(array('data'=>$data));
	}
	
}