<?php 
namespace App\Controllers;
use App\Models\Kontrak;

/**
* This is Example Controller
*/
class Datamaster extends BaseController {

	public function __construct()
{		

	 	$session=session();
	 	if(empty($session->status)){
	 		// echo 0;
	 		header('location:/Login');
        	exit();
	 		// $this->cekLogin();
	 		// return redirect('Login');
	 		 // header("Location: /Login");
	 		// return redirect()->to(site_url("Login"));
			
	 	}else{
	 		// echo 1;
	 	}

	 // 		echo view('template/default_sidenavs');
		// 	echo view('template/default_topnavs.php');
	 }

	 // function cekLogin(){
	 // 	    return redirect('login');
	 // 		// return redirect()->to(site_url("Login"));
	  
	 // }

	function index(){
		// if($this->cekLogin()==1){
		
		echo view('adminlite/header');
		echo view('adminlite/home');
		echo view('adminlite/footer');
		// }else{
		// return redirect()->to(site_url("Login"));
		// }
	}
		
	function view_kontrak(){

        $session = session();
        $session->set(array('level'=>'officer'));
        $kontrak = new Kontrak();
        $data= [
        	'data'=>$kontrak->getData()
        ]; 

  
		echo view('adminlite/header');
		echo view('v_kontrak',$data);
		echo view('adminlite/footer');
	}

	function viewEditDataKontrak($idKontrak){
// 		$nomorKontrak=str_replace(".","/",$nomorKontrak);
		// $this->template->write('title', 'Data Kontrak', TRUE);
		// $this->template->write('header', 'Edit Data Kontrak');
		$session= session();
		$kontrak = new Kontrak();
        $data= [
        	'data'=>$kontrak->getKontrakById($idKontrak),
        	'vendor'=>$kontrak->getVendor()
        ]; 

		
		echo view('adminlite/header');
		echo view('v_editDataKontrak',$data);
		echo view('adminlite/footer');
		// $this->template->write_view('content', 'kontrak/v_editDataKontrak',$query, true);

		// $this->template->render();

	}
	
		function simpanDataKontrak(){ // fungsi untuk menyimpan perubhan pada edit kontrak
			$request = \Config\Services::request();

		
         $idKontrak=$request->getPost('idKontrak');
         $tanggalKontrak=$request->getPost('tanggalKontrak');
         $kodeVendor=$request->getPost('kodeVendor');
         $nomorKontrak=$request->getPost('nomorKontrak');
         $kategori=$request->getPost('kategori');
         $tanggalMulai=$request->getPost('tanggalMulai');
		 $tanggalSelesai=$request->getPost('tanggalSelesai');
		 $jangkaWaktuPembayaran=$request->getPost('jangkaWaktuPembayaran');
		 $ketTambahan=$request->getPost('ketTambahan');
		 $lampiran=$request->getPost('lampiran');
		 $judul=$request->getPost('judul');
		 
		 

		 $data=array(
		    'tanggalKontrak'=>$tanggalKontrak,
		 	'tanggalMulai'=>$tanggalMulai,
		 	'tanggalSelesai'=>$tanggalSelesai,
		 	'jangkaWaktuPembayaran'=>$jangkaWaktuPembayaran,
		 	'ketTambahan'=>$ketTambahan,
		 	'kategori'=>$kategori,
		 	'nomorKontrak'=>$nomorKontrak,
		 	'lampiranKontrak'=>$lampiran,
		 	'judul'=>$judul,
		 	
		 	
		 	);
		 

		$db     = \Config\Database::connect();
		$builder = $db->table('kontrak');

		$builder->where('idKontrak', $idKontrak);
		$builder->update($data);

		return redirect()->to(site_url("Datamaster/view_kontrak"));
		}


		function viewDataAmendKontrak(){
			$session = session();
	        $session->set(array('level'=>'officer'));
	        $kontrak = new Kontrak();
	        $data= [
	        	'data'=>$kontrak->getAmendKontrak()
	        ]; 

	        // print_r($data);
	        
			echo view('adminlite/header');
			echo view('v_dataAmendKontrak',$data);
			echo view('adminlite/footer');
		}

		function viewEditDataAmendKontrak($idAmendKontrak){ //  
		$kontrak = new Kontrak();
		
		$data=array(
		"data"=>$kontrak->getEditAmendKontrak($idAmendKontrak),
		"kontrak"=>$kontrak->getDataKontrak()
		);	
		
		echo view('adminlite/header');
		echo view('v_editDataAmendKontrak',$data);
		echo view('adminlite/footer');
		

		// $this->template->write_view('content', 'kontrak/v_editDataAmendKontrak',$query, true);
		

	}
	function simpanDataAmendKontrak(){// fungsi untuk proses simpan update amend kontrak  
		 $request = \Config\Services::request();



         $idAmend=$request->getPost('idAmendKontrak');
		 $idKontrak=$request->getPost('idKontrak');
		 $tanggalAmend=$request->getPost('tanggalAmend');
		 $nomorAmendKontrak=$request->getPost('nomorAmendKontrak');
		 $kategori=$request->getPost('kategori');
		 $tanggalMulai=$request->getPost('tanggalMulai');
		 $tanggalSelesai=$request->getPost('tanggalSelesai');
		 $amandemen=$request->getPost('amandemen');
		 $ketTambahan=$request->getPost('ketTambahan');
		 $lampiran=$request->getPost('lampiran');
		 
          if(empty($tanggalMulai)){
             $tanggalMulai="0000-00-00";
         };
           if(empty($tanggalSelesai)){
             $tanggalSelesai="0000-00-00";
         };
		 	
      
        
		 // $config['upload_path'] = './uploads/';
   //      // set allowed file types
   //       $config['allowed_types'] = 'pdf';
   //      // set upload limit, set 0 for no limit
   //       $config['max_size']    = 0;
 
   //      // load upload library with custom config settings
   //      $this->load->library('upload', $config);
   //      $fileName="";
   //       // if upload failed , display errors
   //      if (!$this->upload->do_upload())
   //      {
   //          $this->data['error'] = $this->upload->display_errors();
   //           $this->data['page_data'] = 'admin/upload_view';
   //           //$this->load->view('admin/admin', $this->data);
   //       }
   //      else
   //      {
   //      	$result = $this->upload->data();
   //          // echo "<pre>";
   //          // print_r($result);
           
   //          $fileName= $result['file_name'];
   //          // echo "</pre>";
   //      }
        
        
        // if($fileName!=""){
 
         $data=array(
		     'nomorAmend'=>$nomorAmendKontrak,
		     'tanggalMulai'=>$tanggalMulai,
		     'tanggalAmend'=>$tanggalAmend,
		 	'tanggalSelesai'=>$tanggalSelesai,
		 	'idKontrak'=>$idKontrak,
		 	'ketTambahan'=>$ketTambahan,
		 	'amendemen'=>$amandemen,
		 	'lampiranAmend'=>$lampiran
		 	);


        $db     = \Config\Database::connect();
		$builder = $db->table('amendkontrak');

		$builder->where('idAmendKontrak', $idAmend);
		$builder->update($data);

		return redirect()->to(site_url("datamaster/viewDataAmendKontrak"));

   //      }else{
   //          $this->db->set(array(
		 //     'nomorAmend'=>$nomorAmendKontrak,
		 //     'tanggalAmend'=>$tanggalAmend,
		 //     'tanggalMulai'=>$tanggalMulai,
		 // 	'tanggalSelesai'=>$tanggalSelesai,
		 // 	'idKontrak'=>$idKontrak,
		 // 	'ketTambahan'=>$ketTambahan,
		 // 	'amendemen'=>$amandemen
		 	 
		 // 	));
		 // 	echo $idAmend;

   //      } 
		 // $this->db->where('idAmendKontrak', $idAmend);
		 // $db=$this->db->update('amendkontrak'); 
        

		 // if($db){
		 //     $this->session->set_flashdata('status', 1);
		 //    redirect("DataMaster/viewEditDataAmendKontrak/".str_replace("/", ".",$idAmend));
		 // }
	}

	function viewTambahDataKontrak(){
		// if($this->session->level=="vendor"){
		//     $query['vendor']= $this->db->query('select * from vendor where kodeVendor='.$this->session->kodevendor.' ')->result();
		// }else{
		//     $query['vendor']= $this->db->get('vendor')->result();
		// }
		$kontrak = new Kontrak();
		$query['vendor']= $kontrak->getVendor();

		echo view('adminlite/header');
		echo view('v_tambahDataKontrak',$query);
		echo view('adminlite/footer');
		
		// $this->template->render();
	}

	function tambahDataKontrak(){ // fungsi untuk proses simpan kontrak baru
 	     $request = \Config\Services::request();	

         $kodeVendor=$request->getPost('kodeVendor');
         $nomorKontrak=$request->getPost('nomorKontrak');
         $tanggalKontrak=$request->getPost('tanggalKontrak');
         $kategori=$request->getPost('kategori');
         $tanggalMulai =$request->getPost('tanggalMulai');
         $tanggalSelesai =$request->getPost('tanggalSelesai');
         $jangkaWaktuPembayaran =$request->getPost('jangkaWaktuPembayaran');
         $ketTambahan =$request->getPost('ketTambahan');
         $lampiran =$request->getPost('lampiran');
         $judul=$request->getPost('judul');
		 
			

		 $data=array(
		    'kodeVendor'=>$kodeVendor,
		    'nomorKontrak'=>$nomorKontrak,
		    'tanggalKontrak'=>$tanggalKontrak,
		 	'tanggalMulai'=>$tanggalMulai,
		 	'tanggalSelesai'=>$tanggalSelesai,
		 	'jangkaWaktuPembayaran'=>$jangkaWaktuPembayaran,
		 	'ketTambahan'=>$ketTambahan,
		 	'kategori'=>$kategori,
            'validasi'=>1,
		 	'lampiranKontrak'=>$lampiran,
		 	'judul'=>$judul
		 	);
		  
  		$db  = \Config\Database::connect();
  		$db=$db->table('kontrak')->insert($data);
         

		 if($db){
		 	$session = \Config\Services::session();
		     $session->setFlashdata('status', 1);
			 return redirect()->to(site_url("Datamaster/view_kontrak"));
		
		 }
	}

	function deleteDataKontrak($idKontrak){
		
		$db     = \Config\Database::connect();
		$builder = $db->table('kontrak');
		$builder->where('idKontrak', $idKontrak);
		$builder->delete();
	     // $this->db->delete('kontrak', array('idKontrak' => $idKontrak));

	     return redirect()->to(site_url("Datamaster/view_kontrak"));
	}

	function deleteDataAmendKontrak($idKontrak){
		
		$db     = \Config\Database::connect();
		$builder = $db->table('amendkontrak');
		$builder->where('idAmendKontrak', $idKontrak);
		$builder->delete();
	     // $this->db->delete('kontrak', array('idKontrak' => $idKontrak));

	     return redirect()->to(site_url("Datamaster/viewDataAmendKontrak"));
	}
	function viewTambahDataAmendKontrak(){
		
		
        // $data['data']=$this->db->query('select * from kontrak order by idKontrak')->result();	
        
        // if($this->session->level=="vendor"){
        //     $data['kontrak']= $this->db->query('select * from kontrak where kodeVendor='.$this->session->kodevendor.' ')->result();    
        // }else{
        //     $data['kontrak']= $this->db->get('kontrak')->result();    
        // }
      	$kontrak = new Kontrak();
		$query['data']= $kontrak->getDataKontrak();

		echo view('adminlite/header');
		echo view('v_tambahDataAmendKontrak',$query);
		echo view('adminlite/footer');

      	
		// $this->template->write_view('content', 'kontrak/v_tambahDataAmendKontrak', $data, true);

		// $this->template->render();

	}

	function tambahDataAmendKontrak(){ // fungsi untuk proses simpan kontrak baru
		 $request = \Config\Services::request();	

         $idKontrak=$request->getPost('idKontrak');
		 $nomorAmendKontrak=$request->getPost('nomorAmendKontrak');
		 $tanggalAmend=$request->getPost('tanggalAmend');
		 $kategori=$request->getPost('kategori');
		 $tanggalMulai=$request->getPost('tanggalMulai');
		 $tanggalSelesai=$request->getPost('tanggalSelesai');
		 $amandemen=$request->getPost('amandemen');
		 $ketTambahan=$request->getPost('ketTambahan');
		 $lampiran=$request->getPost('lampiran');

		
         if(empty($tanggalMulai)){
             $tanggalMulai="0000-00-00";
         };
           if(empty($tanggalSelesai)){
             $tanggalSelesai="0000-00-00";
         };
		 	
		  $data=array(
		     'nomorAmend'=>$nomorAmendKontrak,
		     'tanggalMulai'=>$tanggalMulai,
		 	'tanggalSelesai'=>$tanggalSelesai,
		 	'tanggalAmend'=>$tanggalAmend,
		 	'idKontrak'=>$idKontrak,
		 	'ketTambahan'=>$ketTambahan,
		 	'amendemen'=>$amandemen,
		 	'validasi'=>1,
		 	'lampiranAmend'=>$lampiran
		 	);
		 	
		  
     	$db  = \Config\Database::connect();
  		$db=$db->table('amendkontrak')->insert($data);
         

		 if($db){
		 	$session = \Config\Services::session();
		     $session->setFlashdata('status', 1);
			 return redirect()->to(site_url("Datamaster/viewDataAmendKontrak"));
		
		 }

	}
	function viewDataVendor(){
	    
		// $this->template->write('title', 'Data Vendor', TRUE);
		// $this->template->write('header', 'Tabel Vendor');
		
		// for ($i=0; $i <100 ; $i++) { 
		// 	$this->db->insert('vendor',array('kodeVendor'=>$i,'namaVendor'=>'a','keterangan'=>'y'));
		// }
		// $this->load->model('M_Vendor');
		// $data['data']= $this->M_Vendor->getAll();
			$session = session();
	        $session->set(array('level'=>'officer'));
	        $kontrak = new Kontrak();
	        $data= [
	        	'data'=>$kontrak->getVendor()
	        ]; 

	        // print_r($data);
	        
			echo view('adminlite/header');
			echo view('v_dataVendor',$data);
			echo view('adminlite/footer');

		

		// 

		// $this->template->render();
	    

	}
	function viewEditDataVendor($idVendor){
		

	        $kontrak = new Kontrak();
	        $data= [
	        	'data'=>$kontrak->getVendorById($idVendor)
	        ]; 

	        // print_r($data);
	        
			echo view('adminlite/header');
			echo view('v_editDataVendor',$data);
			echo view('adminlite/footer');

	}

	function simpanDataVendor(){ // fungsi untuk menyimpan update data vendor
    
		 $request = \Config\Services::request();
		 $kv=$request->getPost('kodeVendor');
		 $nv=$request->getPost('namaVendor');
		 $k=$request->getPost('keterangan');
		 $data=array(
		 	'namaVendor'=>$nv,
		 	'keterangan'=>$k
		 );
		
		$db      = \Config\Database::connect(); 
		$builder = $db->table('vendor');

		$builder->where('kodeVendor', $kv);
		$builder->update($data);
// 		$session= session();
// 		if($builder->update($data)){
// 		    $session->set(array('simpan'=>1));
// 		}else{
// 		    $session->set(array('simpan'=>0));
// 		}
		

		return redirect()->to(site_url("datamaster/viewDataVendor"));

	}
	
	function deleteDataVendor($kv){
	     $db     = \Config\Database::connect();
		$builder = $db->table('vendor');
		$builder->where('kodeVendor', $kv);
		$builder->delete();

	     return redirect()->to(site_url("datamaster/viewDataVendor"));
	}


	function tambahDataVendor(){ // fungsi untuk proses simpan tambah vendor

		 $request = \Config\Services::request();
		 $kv=$request->getPost('kodeVendor');
		 $nv=$request->getPost('namaVendor');
		 $k=$request->getPost('keterangan');

	    $data = array(
               'kodeVendor' => $kv ,
               'namaVendor' => $nv,
               'keterangan' => $k
            );

        $db  = \Config\Database::connect();
  		$db=$db->table('vendor')->insert($data);
  		  return redirect()->to(site_url("datamaster/viewDataVendor"));

	}

	function viewTambahDataVendor(){
		
			echo view('adminlite/header');
			echo view('v_tambahDataVendor');
			echo view('adminlite/footer');

		


	}
}