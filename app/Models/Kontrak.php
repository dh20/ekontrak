<?php 
namespace App\Models;
use CodeIgniter\Model;
 
class Kontrak extends Model
{
    public $kontrak = 'kontrak';
     
    // public __construct(){
    // 	$this->db = db_connect();
    // }

    public function getDataKontrakAmend(){
    	$kontrak= $this->db->query('SELECT * from kontrak k join vendor v on (k.kodeVendor=v.kodeVendor) ')->getResult();
    	$amendKontrak= $this->db->query('SELECT * from amendkontrak ')->getResult();
    	// echo count($amendKontrak);
    	$data=array();
    	$ii=0;
    	$xx=0;
    	for ($i=0; $i < count($kontrak) ; $i++) { 
    		// echo '<br>'.$kontrak[$i]->idKontrak;
    		$a=0;
    		// echo "Kontrak".$kontrak[$i]->idKontrak;
			$amend=array();
            $lampiranAmendKontrak=array();
            $resume=array();
            $amendemen=array();

    		for ($x=0; $x < count($amendKontrak); $x++) { 
    				// echo '<br>'.$amendKontrak[$x]->idAmendKontrak;
    			    

    				if($kontrak[$i]->idKontrak==$amendKontrak[$x]->idKontrak){
    					// echo 'Amend'.$kontrak[$i]->idKontrak.'-'.$amendKontrak[$x]->idAmendKontrak." | ";
    					$amend[$a]=$amendKontrak[$x]->idAmendKontrak;
                        $lampiranAmendKontrak[$a]=$amendKontrak[$x]->lampiranAmend;
                        $resume[$a]=$amendKontrak[$x]->ketTambahan;
                        $amendemen[$a]=$amendKontrak[$x]->amendemen;
						
						

    					$xx++;
    					$a++;
    				}
    				
    							
    				// $data[$ii][$xx]=Array($kontrak[$i]->idKontrak=>$amendKontrak[$x]->idKontrak);
					
    				
    		}


    		$data[$ii]=Array(
    						"index"=>$ii,
    						"idKontrak"=>$kontrak[$i]->idKontrak,
    						"nomorKontrak"=>$kontrak[$i]->nomorKontrak,
    						"namaVendor"=>$kontrak[$i]->namaVendor,
    						"tanggalKontrak"=>$kontrak[$i]->tanggalKontrak,
    						"kodeVendor"=>$kontrak[$i]->kodeVendor,
    						"tanggalMulai"=>$kontrak[$i]->tanggalMulai,
    						"tanggalSelesai"=>$kontrak[$i]->tanggalSelesai,
    						"jangkaWaktuPembayaran"=>$kontrak[$i]->jangkaWaktuPembayaran,
    						"lampiranKontrak"=>$kontrak[$i]->lampiranKontrak,
    						"kategori"=>$kontrak[$i]->kategori,
                            "judul"=>$kontrak[$i]->judul,
                            "ketTambahan"=>$kontrak[$i]->ketTambahan,
    						"amendKontrak"=>$amend,
                            "resume"=>$resume,
                            "amendemen"=>$amendemen,
                            "lampiranAmendKontrak"=>$lampiranAmendKontrak
    		);	
    			// $data[$ii][$xx]=Array(
    			// 			"idKontrak"=>$kontrak[$i]->idKontrak
    			// 		);	
    				


    		
    		$xx=0;
    		$ii++;
    		// echo "<br>";
    		// echo "--------------";
    	}


    	// echo json_encode($data);
		return $data;
    }

    public function getData(){
		return $this->db->query('select * from kontrak k join vendor v on (k.kodeVendor=v.kodeVendor) ')->getResult();
    }


    public function getKontrak($id = false)
    {
        if($id === false){
            return $this->findAll();
        }else{
            return $this->getWhere(['idKontrak' => $id]);
        }   
    }

 
    public function getKontrakById($idKontrak){
    //     $query['data'] = $this->db->get_where('kontrak', array('idKontrak' => $idKontrak ))->getResult();
    //     $query['vendor']= $this->db->get('vendor')->getResult();
    // }

        return $this->db->query('select * from kontrak where idKontrak="'.$idKontrak.'" ')->getResult();       
    }

    public function getVendor(){
        return $this->db->query('select * from vendor')->getResult();       
    }
    public function getVendorById($id){
        return $this->db->query('select * from vendor where kodeVendor="'.$id.'"')->getResult();       
    }
    public function getAmendKontrak(){
    return $this->db->query('select k.idKontrak,k.nomorKontrak,k.kodeVendor,ak.* from amendkontrak ak join kontrak k on ak.idKontrak=k.idKontrak  order by idAmendKontrak desc')->getResult();       
    }

    public function getEditAmendKontrak($id){
        return $this->db->query('select * from amendkontrak where idAmendKontrak="'.$id.'" ')->getResult();
    }

    public function getDataKontrak(){
        return $this->db->query('select * from kontrak')->getResult();
    }
}