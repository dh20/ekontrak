-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 18, 2021 at 06:07 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id17366491_ekontrak_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `amendkontrak`
--

CREATE TABLE `amendkontrak` (
  `idAmendKontrak` int(11) NOT NULL,
  `nomorAmend` varchar(100) NOT NULL,
  `tanggalAmend` varchar(30) DEFAULT NULL,
  `tanggalMulai` date DEFAULT NULL,
  `idKontrak` varchar(100) NOT NULL,
  `tanggalSelesai` date DEFAULT NULL,
  `ketTambahan` text NOT NULL,
  `amendemen` int(11) NOT NULL,
  `lampiranAmend` text NOT NULL,
  `validasi` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `id_karyawan` int(11) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kontrak`
--

CREATE TABLE `kontrak` (
  `idKontrak` int(11) NOT NULL,
  `nomorKontrak` varchar(100) NOT NULL,
  `tanggalKontrak` varchar(30) DEFAULT NULL,
  `tanggalMulai` date NOT NULL,
  `lampiranKontrak` tinytext NOT NULL,
  `kodeVendor` varchar(50) NOT NULL,
  `tanggalSelesai` date NOT NULL,
  `jangkaWaktuPembayaran` varchar(20) NOT NULL,
  `ketTambahan` text NOT NULL,
  `kategori` varchar(20) NOT NULL,
  `judul` text DEFAULT NULL,
  `validasi` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `statusvalidasi`
--

CREATE TABLE `statusvalidasi` (
  `id` int(11) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `statusvalidasi`
--

INSERT INTO `statusvalidasi` (`id`, `status`) VALUES
(1, 'Menunggu Validasi'),
(2, 'Validasi Officer'),
(3, 'Validasi Kadis');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `nik` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `level` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`nik`, `nama`, `level`, `password`) VALUES
(1111, 'user', 'user', '6ad14ba9986e3615423dfca256d04e3f'),
(10693, 'BAMBANG CATUR SUPRIYADI', 'kadis', 'fe5c354db77af0e556b039a3adcd280a'),
(12345, 'admin', 'admin', '0192023a7bbd73250516f069df18b500'),
(12642, 'WILUJENG TEGUH  ', 'officer', '89f81969db882c99c3f3748dec339618'),
(13324, 'DARMAYANTO INDRA MUSTAQIM', 'officer', '9b01ba7ace7e66fc850b1e178e8de4e6'),
(13477, 'Dhitta Hananda', 'officer', 'd5bc7c54ae27dfeb4f766ed6bed1a058');

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `kodeVendor` varchar(50) NOT NULL,
  `namaVendor` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `password` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`kodeVendor`, `namaVendor`, `keterangan`, `password`) VALUES
('200834', ' PT. BUANA CENTER SWAKARSA', 'JL.RAYA MERAK KM.115 RAWA ARUM\r\n11111,  CILEGON\r\nTELP 0254 570444, HP 087808122998\r\nFAX 0254 570666 , EMAIL ptbcsina@bcs-logistics.co.id', '5369b642c184a6b759c7729266908201'),
('300022', ' PT. WAHANA SENTANA BAJA', 'AREA PERKANTORAN KRAKATAU ENGINEERING JL. ASIA RAYA KAV.03\r\n42414, KAWASAN INDUSTRI KRAKATAU CILEGON\r\nTELP 0254396375, HP 087871422659\r\nFAX 0254392428, EMAIL wsbclg.marketing@gmail.com', '5c831702ae385fa668c30a701e7f6bfb'),
('300020', 'PT. SANKYU INDONESIA INTERNATIONAL', 'JL.KAWASAN INDUSTRI TERPADU INDONESIA CHINA (KITIC) KAV.20\r\nKEL.NAGASARI KEC.SERANG BARU\r\n17330, BEKASI\r\nTELP 021 50555340,  HP 085946815000\r\nFAX 021 5200741, EMAIL zaedi0028@sankyu.co.id', '6ef4265369b51ce2c509ecb61e4f0a18'),
('300015', 'PT. MULTI SENTANA BAJA', 'NO.11 JL.BRIGJEN KATAMSO TEGAL RATU\r\n42445 CILEGON\r\nTELP 601472, HP 081294640987\r\nFAX 600165, EMAIL msb.stev@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b'),
('200051', ' PT. ADICARAKA TIRTA CONTAINERLINE', ' FORWARDER\r\nJL YOS SUDARSO KAV 89 MITRA SUNTER\r\nBOULEVARD C.2 SUNTER JAYA,\r\n14350,JAKARTA UTARA\r\nTelp 021 6515538, Hp 0818730259 \r\nEmail . project@adicaraka.com \r\nFax 021 6515506', '53e9568258b03e6f98ce8e842ba7d39b'),
('206769', 'PT. KRAKATAU TIRTA OPERASI & PEMELIHARAAN ', 'JL.RAYA ANYER BLOK KAV. A NO.01 KOTA CILEGON\r\n11111 CILEGON\r\nEMAIL krakatau.top@gmail.com, TELP 0254311206', '640459877050d8c6a3bb35ba2e1c031e'),
('206684', 'MERASETI BANTEN LOGISTIK PT', 'CITRA RAYA GARDEN BOULEVARDBBLK M 32 NO.253\r\nKEL.CIAKAR PANONGAN TANGERANG\r\n', '03b35f81c55904d9ff6106d0bf416112');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `amendkontrak`
--
ALTER TABLE `amendkontrak`
  ADD PRIMARY KEY (`idAmendKontrak`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indexes for table `kontrak`
--
ALTER TABLE `kontrak`
  ADD PRIMARY KEY (`idKontrak`);

--
-- Indexes for table `statusvalidasi`
--
ALTER TABLE `statusvalidasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`kodeVendor`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `amendkontrak`
--
ALTER TABLE `amendkontrak`
  MODIFY `idAmendKontrak` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `id_karyawan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kontrak`
--
ALTER TABLE `kontrak`
  MODIFY `idKontrak` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `statusvalidasi`
--
ALTER TABLE `statusvalidasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
